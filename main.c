#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <time.h>
#include <sys/stat.h>
#include <assert.h>
#include <ctype.h>
#include <math.h>
#define NOPARTICIONES 4
#define max_args 13  /*Numero maximo de argumentos (-1) cuando se trate de un comando externo*/
#define maxln_Com_Amb 105 /*Numero de caracteres maximo para comando las variables de ambiente*/
#define max_com_disc 105
int mbrs_creados = 1;
struct Mounted montados;
/*Declarando variables*/
char comando[maxln_Com_Amb]; /*Esta var lee el comando que ingrese el usuario*/
char *args[max_args]; /*en este arreglo se almacenan los argumentos del comando ingresado*/
char *args2[max_args]; /*en este arreglo se almacenan los argumentos del comando ingresado*/
char *args3[max_args]; /*en este arreglo se almacenan los argumentos del comando ingresado*/
char *args4[max_args]; /*en este arreglo se almacenan los argumentos del comando ingresado*/
char *args5[max_args]; /*en este arreglo se almacenan los argumentos del comando ingresado*/
char comandoDisc[max_com_disc];

/*Declarando variables de ambiente*/
char SHELL[maxln_Com_Amb];
char PATH[maxln_Com_Amb];
char HOME[maxln_Com_Amb];
char PWD[maxln_Com_Amb];

/*Declarando cabeceras de funciones*/
void separaArgs(void); /*Esta funcion separa la cadena ingresada por el usuario en
  palabras individuales, la 1ra palabra sera considerada comando y el resto sus argumentos*/
void listaDir(void); /*Esta func ejecuta el comando dir*/
void eco(void); /*Esta func ejecuta el comando echo*/
void comExterno(void); /*Esta func ejecuta lo que se considere comandos externos*/
void crearDisco(char *comandoDisco);
void separaiguales(char *recibe);
void separaiguales2(char *recibe2);
void MKDISK(int  size, char* path);
void MKDISK2(int  siz,char *unit, char* path);
void separaiguales3(char *recibe3);
void separaiguales4(char *recibe4);
//estructuras del disco duro
typedef struct EXTENDED_BOOT_RECORD{
    char part_status;
    char part_fit;
    int part_start;
    int part_size;
    int part_next;
    char part_name[16];
}EBR;
typedef struct  PARTITION{
    char part_status;//Activa, Desactiva
    char part_type;//Primara, Extendida
    char part_fit;//Best, First, Worst
    int part_start;//inicio de particion
    int part_size;
    char part_name[16];
}Partition;
typedef struct MASTERBOOTRECORD{
    int mbr_tam;
    char mbr_fecha_creacion[19];
    int mbr_disk_signature;
    Partition mbr_partition[NOPARTICIONES];
}MBR;
struct struct_disco{
    char *nombre;
    char tamano[9];
    char particiones[9];
    char puntero[9];
};
typedef struct MountedParts{
    char name[4];
    int bit_start;
    struct MountedPartition* siguiente;
}MountedPartition;
typedef struct MountedDisk{
    char path[200];
    char letter;
    char count;
    MountedPartition* first;
    struct MountedDisk* siguiente;
}MountedDisk;
typedef struct Mounted{
    char current_letter;
    MountedDisk* first;
}Mounted;



//variables globales
char sistema[8]="sistema/";
char sistema_reporte[17]="reporte/";
char directorio[58];
char directorio_reporte[640];
char buffer[1];
int ifor=0;
DIR *d;
struct dirent *di;
char fecha[16];
int login=0;
//establecer directorio de disco

void settime(){
    time_t  t = time(0);
    struct tm *tlocal = localtime(&t);
    strftime(fecha,16,"%d/%n*%y %H:%M",tlocal);
}


void set_disco(char nombre[50]){
    sprintf(directorio, "%s%s", sistema, nombre);
}

void limpiarCadena(char *cadena, int tam){
    while(tam>=0){
        cadena[tam] = '\0';
        tam--;
    }
}

char* timeToChar(){
        char* t = malloc(19);
        time_t tiempo = time(0);
        struct tm *tlocal = localtime(&tiempo);
        strftime(t, 19, "%d/%m/%y--%H:%M:%S", tlocal);
        //printf("%s",t);
        return t;
}

char *carpeta(char *path){
    int tam = strlen(path);
    char *nueva;
    strcpy(nueva, path);
    char caracter  = '\0';
    while(caracter !='/'){
        nueva[tam] = '\0';
        tam--;
        caracter = nueva[tam];
    }
    return nueva;
}
void MKDISK(int  siz, char* path){//SIZE EN BYTES
    printf("Iniciando la creacion..\n");
    int size= siz*1024;
    MBR mbr;
    mbr.mbr_tam = size;
    strcpy(mbr.mbr_fecha_creacion,timeToChar());
    mbr.mbr_disk_signature = mbrs_creados;
    mbrs_creados++;
    int i ;
    for(i= 0; i<NOPARTICIONES;i++){
       mbr.mbr_partition[i].part_fit=NULL;
       strcpy(mbr.mbr_partition[i].part_name, "");
       mbr.mbr_partition[i].part_size= NULL;
       mbr.mbr_partition[i].part_start=NULL;
       mbr.mbr_partition[i].part_status= NULL;
       mbr.mbr_partition[i].part_type=NULL;
    }
    FILE *archivo = fopen(path, "w+b");
    fclose(archivo);
    archivo = fopen(path, "rb+");
    rewind(archivo);
    fwrite(&mbr, sizeof(MBR), 1, archivo);
    size = size-sizeof(MBR);
    while(size>0){
        fputc('0',archivo);
        size--;
    }
    fclose(archivo);
    printf("disco creado..\n");
}

void MKDISK2(int  siz,char *unit, char* path){//SIZE EN BYTES
    printf("Iniciando la creacion del disco por favor espere...\n");
    if(strcmp(unit,"m")==0)
    {
        int size= siz*1024*1024;
        MBR mbr;
        mbr.mbr_tam = size;
        strcpy(mbr.mbr_fecha_creacion,timeToChar());
        mbr.mbr_disk_signature = mbrs_creados;
        mbrs_creados++;
        int i ;
        for(i= 0; i<NOPARTICIONES;i++){
           mbr.mbr_partition[i].part_fit=NULL;
           strcpy(mbr.mbr_partition[i].part_name, "");
           mbr.mbr_partition[i].part_size= NULL;
           mbr.mbr_partition[i].part_start=NULL;
           mbr.mbr_partition[i].part_status= NULL;
           mbr.mbr_partition[i].part_type=NULL;
        }
        FILE *archivo = fopen(path, "w+b");
        fclose(archivo);
        archivo = fopen(path, "rb+");
        rewind(archivo);
        fwrite(&mbr, sizeof(MBR), 1, archivo);
        size = size-sizeof(MBR);
        while(size>0){
            fputc('0',archivo);
            size--;
        }
        fclose(archivo);
        printf("disco creado..\n");
    }else if(strcmp(unit,"k")==0)
    {
        int size= siz*1024;
        MBR mbr;
        mbr.mbr_tam = size;
        strcpy(mbr.mbr_fecha_creacion,timeToChar());
        mbr.mbr_disk_signature = mbrs_creados;
        mbrs_creados++;
        int i ;
        for(i= 0; i<NOPARTICIONES;i++){
           mbr.mbr_partition[i].part_fit=NULL;
           strcpy(mbr.mbr_partition[i].part_name, "");
           mbr.mbr_partition[i].part_size= NULL;
           mbr.mbr_partition[i].part_start=NULL;
           mbr.mbr_partition[i].part_status= NULL;
           mbr.mbr_partition[i].part_type=NULL;
        }
        FILE *archivo = fopen(path, "w+b");
        fclose(archivo);
        archivo = fopen(path, "rb+");
        rewind(archivo);
        fwrite(&mbr, sizeof(MBR), 1, archivo);
        size = size-sizeof(MBR);
        while(size>0){
            fputc('0',archivo);
            size--;
        }
        fclose(archivo);
        printf("disco creado con EXITO..\n");
    }else
    {
         printf("La sintaxis que ha ingresado es Incorrecta\n");
    }

}

void FDISK(int siz, char *unit, char* path, char type, char fit, int del,int add, char* name){
    FILE *disco = NULL;
    disco = fopen(path, "rb+");
    if(disco!=NULL){
        if(strcmp(unit,"m")==0)
        {
            int size=siz*1024*1024;
            if(del){

            }else if(add){

            }else{
                Partition new_part;
                MBR mbr;
                fread(&mbr, sizeof(MBR),1, disco);
                int pos = 0;
                while(mbr.mbr_partition[pos].part_status=='A'&&pos<NOPARTICIONES){
                    pos++;
                }
                if(pos>=NOPARTICIONES){
                    printf("Limite de particiones alcanzado\n");
                }else{
                    int position = first_valid_position(size,disco);
                    if(position!=-1){//haciendo la partición si hay espacio
                        new_part.part_start = position;
                        new_part.part_status = 'A';
                        new_part.part_type = type;
                        new_part.part_fit = fit;
                        new_part.part_size = size;
                        strcpy(new_part.part_name,name);
                        mbr.mbr_partition[pos]= new_part;
                        rewind(disco);
                        fwrite(&mbr, sizeof(MBR), 1, disco);//actualiza info el mbr
                        //creando super bloque
                        printf("Particion Creada Con EXITO\n");
                        fclose(disco);
                       }else{
                        printf("Sin espacio suficiente para realizar la particion...\n");
                    }
                }
            }
        }else if(strcmp(unit,"k")==0)
        {
            int size=siz*1024;
            if(del){

            }else if(add){

            }else{
                Partition new_part;
                MBR mbr;
                fread(&mbr, sizeof(MBR),1, disco);
                int pos = 0;
                while(mbr.mbr_partition[pos].part_status=='A'&&pos<NOPARTICIONES){
                    pos++;
                }
                if(pos>=NOPARTICIONES){
                    printf("Limite de particiones alcanzado\n");
                }else{
                    int position = first_valid_position(size,disco);
                    if(position!=-1){//haciendo la partición si hay espacio
                        new_part.part_start = position;
                        new_part.part_status = 'A';
                        new_part.part_type = type;
                        new_part.part_fit = fit;
                        new_part.part_size = size;
                        strcpy(new_part.part_name,name);
                        mbr.mbr_partition[pos]= new_part;
                        rewind(disco);
                        fwrite(&mbr, sizeof(MBR), 1, disco);//actualiza info el mbr
                        //creando super bloque
                        printf("Particion Creada Con EXITO\n");
                        fclose(disco);
                       }else{
                        printf("Sin espacio suficiente para realizar la particion...\n");
                    }
                }
            }

        }else
        {
               printf("Sintaxis incorrecta para crear");
        }
     //termina el primer if del disco
     }
}

void FDISK2(int siz, char* path, char* name){
    FILE *disco = NULL;
    disco = fopen(path, "rb+");
    if(disco!=NULL){
                int size=siz*1024;

                    Partition new_part;
                    MBR mbr;
                    fread(&mbr, sizeof(MBR),1, disco);
                    int pos = 0;
                    while(mbr.mbr_partition[pos].part_status=='A'&&pos<NOPARTICIONES){
                        pos++;
                    }
                    if(pos>=NOPARTICIONES){
                        printf("Limite de particiones alcanzado\n");
                    }else{
                        int position = first_valid_position(size,disco);
                        if(position!=-1){//haciendo la partición si hay espacio
                            new_part.part_start = position;
                            new_part.part_status = 'A';
                            new_part.part_type = 'P';
                            new_part.part_fit = 'BF';
                            new_part.part_size = size;
                            strcpy(new_part.part_name,name);
                            mbr.mbr_partition[pos]= new_part;
                            rewind(disco);
                            fwrite(&mbr, sizeof(MBR), 1, disco);//actualiza info el mbr
                            //creando super bloque
                            printf("Particion Creada Con EXITO\n");
                            fclose(disco);
                           }else{
                            printf("Sin espacio suficiente para realizar la particion...\n");
                        }
                    }
     //termina el primer if del disco
     }
}


int first_valid_position(int required, FILE *file){//debe estar abierto el archivo
    rewind(file);
    fseek(file, sizeof(MBR), SEEK_SET);
    int current = ftell(file);//le manda fseek a ftell lo que encuentre en este caso el tamaño
    int freespace = 0;
    while(!feof(file)&&freespace<required){
        char c = fgetc(file);
        if(c=='0'){
            freespace++;
        }else{
            freespace = 0;
            current = ftell(file);
        }
    }
    if(freespace>=required){
        return current;
    }else{
        return -1;
    }
}

Partition buscar_particion_nombre(char *disc, char *name){
    FILE *disco = NULL;
    Partition part;
    part.part_start =0;
    disco = fopen(disc,"rb+");
    if(disco!=NULL){
        MBR mbr;
        fread(&mbr, sizeof(MBR),1,disco);
        int i;
        for(i = 0;i<NOPARTICIONES;i++){
            if(!strcmp(mbr.mbr_partition[i].part_name,name)){
                return mbr.mbr_partition[i];
            }
        }
        fclose(disco);
        return part;
    }else{
        printf("Ese disco no existe\n");
        return part;
    }
}

MountedDisk *b_disco_montado(char *path){
    MountedDisk *aux = montados.first;
    while(aux!=NULL){
        if(!strcmp(aux->path,path)){
            return aux;
        }
        aux = aux->siguiente;
    }
    return NULL;
}

void MOUNT(char *path, char *name){
    Partition part = buscar_particion_nombre(path,name);//se busca el archivo determina si existe
    if(part.part_start!=0){
        if(montados.first==NULL){
            montados.first = (MountedDisk*)malloc(sizeof(MountedDisk));
            strcpy(montados.first->path,path);
            montados.first->count='1';
            montados.first->letter = montados.current_letter;
            montados.first->first = NULL;
            montados.current_letter++;
            Montar_Particion(montados.first,part.part_start);
        }else{
            MountedDisk* actual = b_disco_montado(path);
            if(actual==NULL){
                MountedDisk* nuevo = (MountedDisk*)malloc(sizeof(MountedDisk));
                strcpy(nuevo->path,path);
                nuevo->count='1';
                nuevo->letter = montados.current_letter;
                nuevo->first = NULL;
                montados.current_letter++;
                nuevo->siguiente = montados.first;
                montados.first = nuevo;
                Montar_Particion(nuevo,part.part_start);
             }else{
                Montar_Particion(actual,part.part_start);
             }
        }
    }else{
            printf("particion no encontrada \n");
        }
}

void Montar_Particion(MountedDisk *disc, int start_bit){
    if(disc->first==NULL){
        disc->first = (MountedPartition*)malloc(sizeof(MountedPartition));
        disc->first->bit_start = start_bit;
        disc->first->name[0]='v';
        disc->first->name[1]='d';
        disc->first->name[2]=disc->letter;
        disc->first->name[3]=disc->count;
        disc->first->siguiente = NULL;
        disc->count++;
        printf("disco %s\n",disc->first->name);
    }else{
        MountedPartition* nuevo = (MountedPartition*)malloc(sizeof(MountedPartition));
        nuevo->bit_start = start_bit;
        nuevo->name[0]='v';
        nuevo->name[1]='d';
        nuevo->name[2]=disc->letter;
        nuevo->name[3]=disc->count;
        nuevo->siguiente = NULL;
        disc->count++;
        nuevo->siguiente = disc->first;
        disc->first = nuevo;
        printf("disco %s \n",disc->first->name);
    }
}

void UNMOUNT(char *name){
    char letra = name[2];
    char num = name[3];
    MountedDisk *disc = montados.first;
    while(disc!=NULL&&disc->letter!=letra){
        disc=disc->siguiente;
    }
    if(disc!=NULL){
        MountedPartition* part = disc->first;
        MountedPartition* ant = disc->first;
        while(part!=NULL&&part->name[3]!=num){
            ant = part;
            part = part->siguiente;
        }
        if(part!=NULL){
            ant->siguiente = part->siguiente;
            free(part);
            printf("Desmontado con EXITO\n");
        }else{
            printf("Particion no encontrada\n");
        }
    }else{
        printf("Disco no encontrado \n");
    }
}

void Rep_mbr(char *path,char *tipo, char* name){

if(strcmp(tipo,"mbr")==0)
{
    char letra = name[2];
    char num = name[3];
    MountedDisk *disc = montados.first;
    while(disc!=NULL&&disc->letter!=letra){
        disc=disc->siguiente;
    }
    if(disc!=NULL){
        MountedPartition* part = disc->first;
        while(part!=NULL&&part->name[3]!=num){
           part = part->siguiente;
        }
        if(part!=NULL){//si se encuentra esa particion
            FILE *file = fopen(disc->path,"rb+");
            rewind(file);
            MBR mbr;
            fread(&mbr, sizeof(MBR), 1, file);
            FILE* archivo = fopen(path,"w");
            fprintf(archivo,"Digraph g{ \n node[shape = record];\n");
            fprintf(archivo, "struct1[shape = record, label = \"{");
            char aux[200];
            fprintf(archivo,"{  Nombre  |  Valor  }|");
            fprintf(archivo,"{mbr_tamaño | %d }|",mbr.mbr_tam);
            fprintf(archivo,"{mbr_fecha_creacion | %s}|",mbr.mbr_fecha_creacion);
            fprintf(archivo,"{mbr_disk_signature | %d}|",mbr.mbr_disk_signature);
            int i = 0;
            for(i= 0;i<4;i++){
                if(mbr.mbr_partition[i].part_start!=0){
                    fprintf(archivo,"{part_status_%d | %c }|", i+1, mbr.mbr_partition[i].part_status);
                    fprintf(archivo,"{part_type_%d | %c }|", i+1, mbr.mbr_partition[i].part_type);
                    fprintf(archivo,"{part_fit_%d | %c }|", i+1, mbr.mbr_partition[i].part_fit);
                    fprintf(archivo,"{part_start_%d | %d}|", i+1, mbr.mbr_partition[i].part_start);
                    fprintf(archivo,"{part_size_%d | %d }|", i+1, mbr.mbr_partition[i].part_size);
                    fprintf(archivo,"{part_name_%d | %s }|", i+1, mbr.mbr_partition[i].part_name);
                }
            }
            fprintf(archivo, "}\"];\n}");
            fclose(archivo);
            system("dot -Tpng reporte.dot -o grafico.png");
            //system("showtell -grafico.png");
            //system(cmd);
            return 0;
        }else{
            printf("Particion no encontrada\n");
            return 0;
        }
    }else{
        printf("Disco no encontrado \n");
        return 0;
    }

}else if(strcmp(tipo,"disk")==0)
 {
    char letra = name[2];
    char num = name[3];
    MountedDisk *disc = montados.first;
    while(disc!=NULL&&disc->letter!=letra){
        disc=disc->siguiente;
    }
    if(disc!=NULL){
        MountedPartition* part = disc->first;
        while(part!=NULL&&part->name[3]!=num){
           part = part->siguiente;
        }
        if(part!=NULL){//si se encuentra esa particion
            FILE *file = fopen(disc->path,"rb+");
            rewind(file);
            MBR mbr;
            fread(&mbr, sizeof(MBR), 1, file);
            FILE* archivo = fopen(path,"w");
            fprintf(archivo,"Digraph G{ \n subgraph cluster_0 {\n");
            fprintf(archivo, "node [shape = box, style =filled,label=""MBR" "] a;\n");

            int i = 0;
            for(i= 0;i<4;i++){
                if(mbr.mbr_partition[i].part_start!=0){

                    if(strcmp( mbr.mbr_partition[i].part_type,"P")==0)
                    {
                        fprintf(archivo, "node [shape = box, style =filled,label=""Primaria""] b;\n");
                    }
                }
            }
            fprintf(archivo, "}\n a; \n b; \n}");
            fclose(archivo);
            system("dot -Tpng reporte2.dot -o grafico2.png");
            //system("showtell -grafico.png");
            //system(cmd);
            return 0;
        }else{
            printf("Particion no encontrada\n");
            return 0;
        }
    }else{
        printf("Disco no encontrado \n");
        return 0;
    }



 }else{
    printf("La sintaxis es incorrecta para generar un reporte");
   }

}


int main(void){
    int continuar =1;
    montados.first = NULL;
    montados.current_letter = 'a';
    //iniciando variables de ambiente
    getcwd(PWD,maxln_Com_Amb);
    strcpy(PATH,getenv("PATH"));
    strcpy(HOME,PWD);
    strcpy(SHELL,PWD);

    //ciclo principal
    do{
        printf("%s>",PWD);
        __fpurge(stdin); /*Limpiando el buffer de entrada de teclado*/
        memset(comando,'\0',maxln_Com_Amb);//borramos contenido previo
        scanf("%[^\n]s",comando);//recibimos comandos

        //actuar al recibir comandos
        if(strlen(comando)>0){
            separaArgs();//separa comandos de argumentos

            if(strcmp(args2[0],"mkdisk")==0){

                    char *ja=args2[1];//separando argumentos por signo =
                    separaiguales(ja);

                     char *jo =args2[2];
                     separaiguales2(jo);

                     char *je =args2[3];
                     separaiguales3(je);

                     char *ji=args2[4];
                     separaiguales4(ji);

                printf("Direccion: %s, tamaño: %s unidad: %s Nombre: %s\n",args[1],args3[1],args4[1],args5[1]);
       if(args4[1]==NULL)
       {
           if(args[1] && args3[1]){
        //verifica la sintaxis para crear un disco

                if(strcmp(args[0],"-path")==0 && strcmp(args3[0],"-size")==0){

                    struct struct_disco tdisco;
                    tdisco.nombre = args[1];
                    int tamano = atoi(args3[1]);
                    MKDISK(tamano,tdisco.nombre);

                }
                else if(strcmp(args3[0], "-path")==0 && strcmp(args[0],"-size")==0){
                            struct struct_disco tdisco;
                            tdisco.nombre = args3[1];
                            int tamano = atoi(args[1]);
                            MKDISK(tamano,tdisco.nombre);

                        }

                else{
                    printf("Sintaxis incorrecta para crear un nuevo Disco\n");
                }

            }
            else{
                            printf("Sintaxis incorrecta para crear un nuevo Disco\n");
         }

       }else{

           if(args[1] && args3[1]&& args4[1])
           {
               if(strcmp(args[0], "-size")==0 && strcmp(args3[0], "-path")==0 && strcmp(args4[0],"-unit")==0){
                   struct struct_disco tdisco;
                   tdisco.nombre = args3[1];
                   int tamano = atoi(args[1]);
                   char *unit = args4[1];
                   printf("Unidad: %s",unit);
                   MKDISK2(tamano,unit,tdisco.nombre);

               }
               else if(strcmp(args[0], "-size")==0 && strcmp(args4[0], "-unit")==0 && strcmp(args3[0],"-path")==0){
                   struct struct_disco tdisco;
                   tdisco.nombre = args[1];
                   int tamano = atoi(args3[1]);
                   int unit=atoi(args4[1]);

                   MKDISK2(tamano,unit,tdisco.nombre);
               }
               else if(strcmp(args[0], "-path")==0 && strcmp(args3[0], "-unit")==0 && strcmp(args4[0],"-size")==0){
                   struct struct_disco tdisco;
                   tdisco.nombre = args[1];
                   int tamano = atoi(args3[1]);
                   int unit=atoi(args4[1]);
                   printf("Unidad: %s",unit);
                   MKDISK2(tamano,unit,tdisco.nombre);
               }
               else if(strcmp(args[0], "-path")==0 && strcmp(args3[0], "-size")==0 && strcmp(args4[0],"-unit")==0){
                   struct struct_disco tdisco;
                   tdisco.nombre = args[1];
                   int tamano = atoi(args3[1]);
                   int unit=atoi(args4[1]);

                   MKDISK2(tamano,unit,tdisco.nombre);
               }
               else if(strcmp(args4[0], "-unit")==0 && strcmp(args3[0], "-size")==0 && strcmp(args[0],"-path")==0){
                   struct struct_disco tdisco;
                   tdisco.nombre = args[1];
                   int tamano = atoi(args3[1]);
                   int unit=atoi(args4[1]);

                   MKDISK2(tamano,unit,tdisco.nombre);
               }
               else if(strcmp(args4[0], "-unit")==0 && strcmp(args[0], "-path")==0 && strcmp(args3[0],"-size")==0){
                   struct struct_disco tdisco;
                   tdisco.nombre = args[1];
                   int tamano = atoi(args3[1]);
                   int unit=atoi(args4[1]);

                   MKDISK2(tamano,unit,tdisco.nombre);
               }
           }else
           {
              printf("Sintaxis incorrecta para crear un nuevo Disco\n");
           }
       }



//Inician otros comandos
            }
            else if(strcmp(comando,"rmdisk")==0){
                if(strcmp(args[2],"-path")){
                    remove(args[2]);
                    printf("Disco borrado con exito: %s \n",args[2]);
                }
                else{
                    printf("Sintaxis incorrecta para eliminar un Disco\n");
                }
            }

            //comando fdisk
            else if(strcmp(comando,"fdisk")==0){
                char *ja=args2[1];//separando argumentos por signo =
                separaiguales(ja);

                 char *jo =args2[2];
                 separaiguales2(jo);

                 char *je =args2[3];
                 separaiguales3(je);

                 char *ji=args2[4];
                 separaiguales4(ji);

            printf("Tamaño: %s, Path: %s Name: %s\n",args[1],args3[1],args4[1]);

                   if(strcmp(args[0], "-size")==0 && strcmp(args3[0], "-path")==0 && strcmp(args4[0],"-name")==0){
                       struct struct_disco tdisco;
                       tdisco.nombre = args3[1];
                       int tamano = atoi(args[1]);
                       FDISK2(tamano,tdisco.nombre,args4[1]);
                   }
            }

            //comando mount
            else if(strcmp(comando,"mount")==0){
                char *ja=args2[1];//separando argumentos por signo =
                separaiguales(ja);

                 char *jo =args2[2];
                 separaiguales2(jo);

                 char *je =args2[3];
                 separaiguales3(je);

                 char *ji=args2[4];
                 separaiguales4(ji);

            printf("Path: %s Name: %s\n",args[1],args3[1]);

                   if(strcmp(args[0], "-path")==0 && strcmp(args3[0], "-name")==0){
                       struct struct_disco tdisco;
                       tdisco.nombre = args3[1];
                       MOUNT(args[1],tdisco.nombre);
                   }else if(strcmp(args[0], "-name")==0 && strcmp(args3[0], "-path")==0){
                       struct struct_disco tdisco;
                       tdisco.nombre = args[1];
                       MOUNT(args3[1],tdisco.nombre);
                   }
            }

            //comando unmount
            else if(strcmp(comando,"unmount")==0){

                char *ja=args2[1];//separando argumentos por signo =
                separaiguales(ja);

                 char *jo =args2[2];
                 separaiguales2(jo);

                 char *je =args2[3];
                 separaiguales3(je);

                 char *ji=args2[4];
                 separaiguales4(ji);

            printf("Id: %s\n",args[1]);

                   if(strcmp(args[0], "-id")==0){

                       UNMOUNT(args[1]);
                   }
            }

            //comando para crear reporte
            else if(strcmp(comando,"rep")==0){
                char *ja=args2[1];//separando argumentos por signo =
                separaiguales(ja);

                 char *jo =args2[2];
                 separaiguales2(jo);

                 char *je =args2[3];
                 separaiguales3(je);

                 char *ji=args2[4];
                 separaiguales4(ji);

            printf("Id: %s Path: %s Name: %s\n",args[1],args3[1],args4[1]);

                   if(strcmp(args[0], "-id")==0 && strcmp(args3[0], "-path")==0 && strcmp(args4[0], "-name")==0){

                       Rep_mbr(args3[1],args4[1],args[1]);
                   }


            }

            //comando para las tablas MBR
            else if(strcmp(comando,"mbr")==0){

            }

            //comando para los reportes de particiones
            else if(strcmp(comando,"disk")==0){

            }

            //comando para ejecutar un script
            else if(strcmp(comando,"exec")==0){

            }

            //comando para salir
            else if(strcmp(comando,"salir")==0){
                continuar=0;
            }

            else comExterno();
        }

    }while(continuar);

  return 0;
}

void separaArgs(void){
  int i;
  for(i=0;i<(max_args-1);i++) args2[i]=NULL; /*Borrar argumento previo que pudiera existir*/
  strtok(comando," "), i=0;
      /*separar palabras individuales usando tokens (espacio vacio)*/
     args2[i]=comando; /*El 1er argumento sera comando por un requerimiento de execvp*/
     while(((args2[++i]=strtok(NULL," "))!=NULL && i<(max_args-2)));
}
void separaiguales(char *recibe){
  int i;
  for(i=0;i<2;i++) args[i]=NULL; /*Borrar argumento previo que pudiera existir*/
   strtok(recibe,"="), i=0;
      /*separar palabras individuales usando tokens (espacio vacio)*/
     args[i]=recibe; /*El 1er argumento sera comando por un requerimiento de execvp*/
     while(((args[++i]=strtok(NULL,"="))!=NULL && i<(max_args-2)));
}
void separaiguales2(char *recibe2){
  int i;
  for(i=0;i<2;i++) args3[i]=NULL; /*Borrar argumento previo que pudiera existir*/
   strtok(recibe2,"="), i=0;
      /*separar palabras individuales usando tokens (espacio vacio)*/
     args3[i]=recibe2; /*El 1er argumento sera comando por un requerimiento de execvp*/
     while(((args3[++i]=strtok(NULL,"="))!=NULL && i<(max_args-2)));
}
void separaiguales3(char *recibe3){
  int i;
  for(i=0;i<2;i++) args4[i]=NULL; /*Borrar argumento previo que pudiera existir*/
   strtok(recibe3,"="), i=0;
      /*separar palabras individuales usando tokens (espacio vacio)*/
     args4[i]=recibe3; /*El 1er argumento sera comando por un requerimiento de execvp*/
     while(((args4[++i]=strtok(NULL,"="))!=NULL && i<(max_args-2)));
}
void separaiguales4(char *recibe4){
  int i;
  for(i=0;i<2;i++) args5[i]=NULL; /*Borrar argumento previo que pudiera existir*/
   strtok(recibe4,"="), i=0;
      /*separar palabras individuales usando tokens (espacio vacio)*/
     args5[i]=recibe4; /*El 1er argumento sera comando por un requerimiento de execvp*/
     while(((args5[++i]=strtok(NULL,"="))!=NULL && i<(max_args-2)));
}
void listaDir(void){
  char ruta[maxln_Com_Amb]; /*Var auxiliar para formar la ruta solicitada por el usuario*/
  int archs; /*Numero de archivos encontrados en el dir indicado*/
  int cnt=-1;
  struct dirent **lista; /*var que guarda la lista de archivos/dirs encontrados*/
  strcpy(ruta,PWD); /*Suponemos que el dir solicitado es de la ruta actual*/
  if(args[1]) strcat(ruta,"/"), strcat(ruta,args[1]);
    /*pero si se trata de una(s) subcarpeta(s) las concatenamos*/
  archs=scandir(ruta,&lista,0,alphasort); /*Mandamos revisar el dir solicitado*/
  if(archs<0) /*En caso de falla enviar mensaje*/
    printf("Error no existe o no se pudo leer [%s]\n",ruta);
  else if(archs==2){ /*Si solo encontro los directorios . (actual) y .. (padre)
    consideraremos que el dir esta vacio a efectos practicos*/
    printf(" El directorio [%s] esta vacio",ruta);
  }
  else{ /*Si se encontra al menos un archivo/directorio mostrarlo*/
    printf(" Archivos y carpetas encontrados en: [%s]\n",ruta);
    while(++cnt<archs)
      if(strcmp(lista[cnt]->d_name,".")!=0 && strcmp(lista[cnt]->d_name,"..")!=0)
        printf(" %s\n",lista[cnt]->d_name);
  }
}


void comExterno(){ /*Ejecutar entradas consideradas comando externos*/
  int pid=0;
  int status;
  pid=fork(); /*Crear un proceso hijo*/
  if(pid<0) printf("Error! no se pudo crear un proceso hijo");
  if (pid==0){
    status=execvp(comando,args); /*Trata de ejecutar el comando y los argumentos que tenga*/
    if(status){
      printf("Error! %s no se reconoce o no se pudo ejecutar\n",comando);
      exit(1); /*Como no se pudo ejecutar el comando cerramos el proceso hijo*/
    }
  }
  else
    wait(NULL); /*esperar a que termine el proceso hijo*/
}
